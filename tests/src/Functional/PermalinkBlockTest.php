<?php

namespace Drupal\Tests\permalink_block\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test if the site still works, the Permalink block can be placed and works.
 *
 * @group permalink_block
 */
class PermalinkBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['system_test', 'block', 'permalink_block'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create administrative user.
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer themes',
      'administer blocks',
      'administer site configuration',
      'view the administration theme',
    ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test that the Permalink block can be placed and works.
   */
  public function testPermalinkBlock() {
    // Test permalink block availability in admin "Place blocks" list.
    \Drupal::service('theme_installer')->install(['bartik', 'seven', 'stark']);
    $theme_settings = $this->config('system.theme');
    foreach (['bartik', 'seven', 'stark'] as $theme) {
      $this->drupalGet('admin/structure/block/list/' . $theme);
      // Configure and save the block.
      $this->drupalPlaceBlock('permalink_block', [
        'region' => 'content',
        'theme' => $theme,
        'label' => 'Permalink block',
        'label_display' => 'visible',
      ]);
      // Set the default theme and ensure the block is placed.
      $theme_settings->set('default', $theme)->save();
      $this->drupalGet('');
      $this->assertRaw('" title="Right-click to ', 'Permalink found as link');

      // Open admin page and check if HTML copy box is placed.
      $this->drupalGet('admin');
      $this->assertSession()->pageTextContains('Permalink block', 'Permalink block found');

    }
  }

}
