/**
 * @file
 * Select all containing text when clicking anything in the copybox.
 */

(function ($, Drupal, window, document) {
  Drupal.behaviors.selectText = {
    attach: function (context, settings) {
      $('#copybox').click(function () {
        window.getSelection().selectAllChildren(document.querySelector('#copybox'));
      });
    }
  };
}(jQuery, Drupal, this, this.document));
