<?php

namespace Drupal\permalink_block\Plugin\Block;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the 'PermaLinkBlock' block.
 *
 * @Block(
 *  id = "permalink_block",
 *  admin_label = @Translation("Permalink block"),
 * )
 */
class PermaLinkBlock extends BlockBase implements
    ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs the permalink_block object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface service.
   * @param \Drupal\Core\Controller\TitleResolverInterface $titleResolver
   *   The title resolver interface service.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, RequestStack $request, RouteMatchInterface $route_match, TitleResolverInterface $titleResolver, CurrentPathStack $current_path, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $this->configFactory = $config_factory;
    $this->requestStack = $request;
    $this->routeMatchInterface = $route_match;
    $this->titleResolverInterface = $titleResolver;
    $this->currentPath = $current_path;
    $this->languageManager = $language_manager;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('title_resolver'),
      $container->get('path.current'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the domain.
    global $base_url;
    // Get the url of the current page.
    $request = $this->requestStack->getCurrentRequest();
    $prefix = $this->getLanguagePrefix();
    $current_path = $prefix . $this->currentPath->getPath($request);
    $query_page = UrlHelper::filterQueryParameters($this->requestStack->getCurrentRequest()->query->all(), ['page']);
    $query = (!empty($query_page['q'])) ? '?page=' . $query_page['q'] : '';
    $link = $base_url . $current_path . $query;

    if (isset($this->configuration['permalink_block_suppress_copybox']) && $this->configuration['permalink_block_suppress_copybox'] != 1) {
      // Get the title of the current page.
      $route_match = $this->routeMatchInterface->getCurrentRouteMatch();
      $page_title = $this->titleResolverInterface->getTitle($request, $route_match->getRouteObject());
      // Get the site name.
      $site_name = $this->configFactory->get('system.site')->get('name');
      // Get the site slogan.
      $slogan = $this->configFactory->get('system.site')->get('slogan');
      // If there is site slogan, we do need a separator.
      $slogan = (!empty($slogan)) ? (' - ') . $slogan : $slogan;
      // If there is no title (e.g. on front page),
      // use the site name(domain) and slogan.
      $page_title = (!is_string($page_title)) ? $site_name . $slogan : $page_title;
      // Prefix the page title with the site name and decode special characters.
      $title = htmlspecialchars_decode($page_title);
      // Construct a link to the source page.
      $encoded = Link::fromTextAndUrl('', Url::fromUri($link)->setOption('attributes', ['title' => $site_name . $slogan]));
      // Put back in the page title. We wanted to exclude it from being encoded.
      $html = $encoded->setText($title);
      $html = $encoded->toRenderable();
      $html = $encoded->toString();
      if (isset($this->configuration['permalink_block_show'])) {
        $display = $this->configuration['permalink_block_show'];
        $linkonly = ($display['link'] == '0') ? '' : $link;
        $fullhtml = ($display['html'] == '0') ? '' : $html;
      }
      else {
        $linkonly = $link;
        $fullhtml = $html;
      }
      $set_link = Link::fromTextAndUrl($this->t('Permalink'), Url::fromUri($link)->setOption('attributes', ['title' => $this->t("Right-click to 'Copy Link' or copy from below")]));
      $build['permalink_block']['#copybox'] = $set_link->toString();
      $build['permalink_block']['#linkonly'] = $linkonly;
      $build['permalink_block']['#fullhtml'] = $fullhtml;
      $build['#attached']['library'][] = 'permalink_block/permalink_block_copybox';
    }
    else {
      $set_link = Link::fromTextAndUrl($this->t('Permalink'), Url::fromUri($link)->setOption('attributes', ['title' => $this->t("Right-click to 'Copy Link': @link", ['@link' => $link])]));
      $build['permalink_block']['#permalink_block_link'] = $set_link->toString();
      $build['#attached']['library'][] = 'permalink_block/permalink_block';
    }

    $build['permalink_block']['#theme'] = 'permalink-block';
    // Use the permalink as a 'bookmark' metatag in the head section.
    $build['#attached']['html_head_link'][] = [
      [
        'rel' => 'bookmark',
        'href' => $link,
      ],
      TRUE,
    ];
    // Cache the block based on the (alias) path.
    $build['#cache'] = ['contexts' => ['url.path']];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['permalink_block_show'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display'),
      '#default_value' => isset($configuration['permalink_block_show']) ? $configuration['permalink_block_show'] : ['link'],
      '#options' => [
        'html' => $this->t('HTML'),
        'link' => $this->t('Link'),
      ],
      '#description' => $this->t("Use the <em>'HTML'</em> snippet for non tech savvy visitors or for convenience. The <em>'link'</em> is for an audience that prefers writing their own HTML."),
    ];

    $form['permalink_block_suppress_copybox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('No copybox'),
      '#default_value' => $this->configuration['permalink_block_suppress_copybox'],
      '#description' => $this->t("Permalink only. If selected the display settings above are ignored."),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (empty(array_filter($form_state->getValue('permalink_block_show')))) {
      $form_state->setErrorByName('permalink_block_show', $this->t("<strong><em>Display</em></strong>: Select at least one checkbox."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['permalink_block_show'] = $values['permalink_block_show'];
    $this->configuration['permalink_block_suppress_copybox'] = $values['permalink_block_suppress_copybox'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguagePrefix() {
    if ($prefixes = $this->configFactory->get('language.negotiation')->get('url.prefixes')) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      if ($prefixes[$language]) {
        return "/" . $prefixes[$language];
      }
    }
    return NULL;
  }

}
