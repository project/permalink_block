Name: Permalink Block (permalink_block)

Author: Martin Postma ('lolandese', https://drupal.org/user/210402)
Version: Drupal: 7.x

D8 Port: Miguel Martins ('zarabatana', https://www.drupal.org/user/859226)
Version: Drupal: 8.x


-- SUMMARY --

A block with a link or HTML code snippet that links to the current page,
providing an easy way for visitors to embed deep links to your pages on their
website.

The URL contains the node or term ID instead of the alias, so the link won't
break if the title changes. Hence the term 'permalink'.

By default, the block is enabled at the bottom of the theme's 'content' region
on all non-admin pages. This shows a permalink to the current page and
optionally a copy box to ease pasting of the necessary HTML into other websites.


-- USAGE --

Despite the absence of a settings page for the Permalink module, the display can
be customized through block settings, a CSS and a template file.

To position the Permalink on the page and exclude it from some pages use the
block settings. You can also select to show only the URL or HTML for a full
link.

To customize the output, edit the module CSS (permalink_block.css and 
permalink_block_copybox.css) and template file (permalink-block.html.twig).

Best practice is to copy CSS code and template files to your used theme to avoid
them to get overwritten if you update the module.

After making your changes, clear the site cache.


-- ROADMAP --

The 7.x version of Permalink Block provides integration for BeautyTips and Popup
modules.
At the moment this version was developed, none of these modules offered yet a
stable Drupal 8 version and for that reason the integration with permalink_block
8.x is postponed.

After integration is provided it will extend the Permalink Block module with
the following functionality:

	- If the BeautyTips or Popup module is enabled, only the text 'Permalink' is
displayed, with a popup that opens on hover.

	- Toggle the popup functionality off/on, disable/enable the BeautyTips or
Popup module.
